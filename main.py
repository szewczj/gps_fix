import gpxpy
import gpxpy.gpx
from time import sleep

file_lezyne = "Zwardon_Moravany_lezyne.gpx"
file_holux = "zwardon-moravany-pozycje.gpx"
file_output = "zwardon_moravany_fixed.gpx"

def get_point_by_time(gpx_data, gmt_time):
    for track in gpx_data.tracks:
        for segment in track.segments:
            for point in segment.points:
                if (point.time == gmt_time):
                    return point
    print ("ERROR - point not found")
    return None




with open(file_lezyne, "r") as gpx_file:
    lezyne_data = gpxpy.parse(gpx_file)

with open(file_holux, "r") as gpx_file:
    holux_data = gpxpy.parse(gpx_file)

for track in lezyne_data.tracks:
    for segment in track.segments:
        for point in segment.points:
            print(point.time)
            holux_point = get_point_by_time(holux_data, point.time)
            point.latitude = holux_point.latitude
            point.longitude = holux_point.longitude

print("processing finished")

with open(file_output, "w") as f:
    f.write(lezyne_data.to_xml())


    
